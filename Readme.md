## running

All tasks:
```sh
ansible-pull --url git@gitlab.com:vderyagin/provision_workstation.git --ask-become-pass
```

All tasks beside ones that require root privileges:
```sh
ansible-pull --url git@gitlab.com:vderyagin/provision_workstation.git --skip-tags root
```

Only tasks that require root privileges:
```sh
ansible-pull --url git@gitlab.com:vderyagin/provision_workstation.git --tags root --ask-become-pass
```

Tasks with given tags:
```sh
ansible-pull --url git@gitlab.com:vderyagin/provision_workstation.git --tags tag1,tag2 [--ask-become-pass]
```

From repository root:
```sh
ansible-playbook local.yml [--tags=tag1,tag2] [--ask-become-pass]
```
